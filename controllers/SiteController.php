<?php

namespace app\controllers;

use app\models\Articles;
use yii\web\Controller;


class SiteController extends Controller
{

    public function actionIndex()
    {
        $articles = Articles::find()->where(['on_main' => 1])->all();
        return $this->render('index', [
            'articles' => $articles,
        ]);
    }

}
