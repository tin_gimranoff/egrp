<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class ArticleController extends Controller
{

    public function actionIndex()
    {
        $article = \app\models\Articles::find()->where(['url' => $_GET['url']])->one();
        if(!$article)
            throw new \yii\web\NotFoundHttpException(404);
        
        Yii::$app->view->title = $article->name; 

        return $this->render('index', [
            'article' => $article,
        ]);
    }
}
