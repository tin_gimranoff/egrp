<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers;

use app\models\Order;
use Yii;
use yii\web\Controller;

/**
 * Description of RosreestrController
 *
 * @author tin
 */
class RosreestrController extends Controller {
    
    public function actionIndex($query) {
        $api = Yii::$app->rosreestr;        
        if(preg_match('/^[0-9]{2}:[0-9]{2}:[0-9]{6,7}:[0-9]{2,}$/', $query)) {            
            $info = $api->api('cadaster/objectInfoFull', ['query' => $query]);
            if($info == false) {
                Yii::$app->view->title = 'Ошибка';
                return $this->render('single_object', [
                    'error' => 'Ошибка соединения с интернетом',
                    'query' => $query,
                    'info' => $info,
                ]);
            }
            if($info && $info['error']) {
                Yii::$app->view->title = 'Ошибка';
                return $this->render('single_object', [
                        'error' => $info['error']['mess'],
                        'query' => $query,
                        'info' => $info,
                ]);
            }
            Yii::$app->view->title = $query . " — " . $info['EGRN']['details']['Адрес (местоположение)'];
            $info_json = json_encode($info);
            $script = <<< JS
                 info_json = $info_json;
JS;
            Yii::$app->view->registerJS(
                $script,
                \yii\web\View::POS_READY
            );
            $payment_status = Yii::$app->request->get('payment', NULL);
            if($payment_status == 'success') {
                $flash = "show_info_modal('Успешная оплата!', 'Ваша оплата прошла успешно, ожидайте ваши файлы на указанную вами почту.')";
                Yii::$app->view->registerJS($flash, \yii\web\View::POS_READY);
            }
            if($payment_status == 'fail') {
                $flash = "show_info_modal('Ошибка', 'В процессе оплаты произошла ошибка')";
                Yii::$app->view->registerJS($flash, \yii\web\View::POS_READY);
            }
            return $this->render('single_object', [
                'query' => $query,
                'info' => $info,
            ]);
        } else {
            $objects = $api->api('cadaster/search', ['query' => $query]);
            if($objects == false) {
                Yii::$app->view->title = 'Ошибка';
                return $this->render('more_objects', [
                    'error' => 'В процессе соединения с интернетом произошла ошибка',
                    'query' => $query,
                ]);
            }
            if($objects && $objects['error']) {
                Yii::$app->view->title = 'Ошибка';
                return $this->render('more_objects', [
                        'error' => $objects['error']['mess'],
                        'query' => $query,
                ]);
            }
            Yii::$app->view->title = $query . " — Поиск информация из ЕГРН по адресу";
            return $this->render('more_objects', [
                'query' => $query,
                'objects' => $objects,
            ]);
        }
    }

    /*public function actionUpdateDocStatus() {
        Yii::trace('start', 'developer');
        $data = Yii::$app->request->post('data', NULL);
        Yii::trace($data, 'developer');
        if($data == NULL)
            Yii::$app->end();
        if($data['document_type'] == 'XZP') {
            $id_key = 'XZP_KEY';
            $status_key = 'XZP_STATUS';
        } elseif ($data['document_type'] == 'SOPP') {
            $id_key = 'SOPP_KEY';
            $status_key = 'SOPP_STATUS';
        }
        $order = Order::find()->where(['transaction_id' => $data['transaction_id'], $id_key => $data['document_id']])->one();
        Yii::trace($order, 'developer');
        $order->{$status_key} = $data['document_status'];
        $order->save();
        Yii::$app->end();
    }*/
    
}
