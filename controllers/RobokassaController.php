<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Order;
use yii\web\View;

/**
 * Description of RosreestrController
 *
 * @author tin
 */
class RobokassaController extends Controller {

    public function actionRegisterPayment() {
        $info_json = Yii::$app->request->post('info_json', NULL);
        $xzp = Yii::$app->request->post('xzp', NULL);
        $sopp = Yii::$app->request->post('sopp', NULL);
        $email = Yii::$app->request->post('email', NULL);
        $zip_file = Yii::$app->request->post('zip_file', NULL);
        $human_file = Yii::$app->request->post('human_file', NULL);
        $xml_file = Yii::$app->request->post('xml_file', NULL);
        //var_dump($info_json);
        $order = new Order;
        $order->email = $email;
        $order->region = (isset($info_json['region'])) ? $info_json['region'] : NULL;
        $order->CADNOMER = (isset($info_json['EGRN']['object']['CADNOMER'])) ? $info_json['EGRN']['object']['CADNOMER'] : NULL;
        $order->ADDRESS = (isset($info_json['EGRN']['object']['ADDRESS'])) ? $info_json['EGRN']['object']['ADDRESS'] : NULL;
        $order->AREA = (isset($info_json['EGRN']['object']['AREA'])) ? $info_json['EGRN']['object']['AREA'] : NULL;
        $order->CATEGORY = (isset($info_json['EGRN']['object']['CATEGORY'])) ? $info_json['EGRN']['object']['CATEGORY'] : NULL;
        $order->object_type = (isset($info_json['EGRN']['details']['Тип объекта'])) ? $info_json['EGRN']['details']['Тип объекта'] : NULL;
        $order->object_status = (isset($info_json['EGRN']['details']['Статус объекта'])) ? $info_json['EGRN']['details']['Статус объекта'] : NULL;
        $order->date_of_cadastral_registration = (isset($info_json['EGRN']['details']['Дата постановки на кадастровый учет'])) ? $info_json['EGRN']['details']['Дата постановки на кадастровый учет'] : NULL;
        $order->land_category = (isset($info_json['EGRN']['details']['Категория земель'])) ? $info_json['EGRN']['details']['Категория земель'] : NULL;
        $order->permitted_use = (isset($info_json['EGRN']['details']['Разрешенное использование'])) ? $info_json['EGRN']['details']['Разрешенное использование'] : NULL;
        $order->area_num = (isset($info_json['EGRN']['details']["Площадь ОКС'a"])) ? $info_json['EGRN']['details']["Площадь ОКС'a"] : NULL;
        $order->unit = (isset($info_json['EGRN']['details']['Единица измерения (Код)'])) ? $info_json['EGRN']['details']['Единица измерения (Код)'] : NULL;
        $order->cadastral_sum = (isset($info_json['EGRN']['details']['Кадастровая стоимость'])) ? $info_json['EGRN']['details']['Кадастровая стоимость'] : NULL;
        $order->value_date = (isset($info_json['EGRN']['details']['Дата определения стоимости'])) ? $info_json['EGRN']['details']['Дата определения стоимости'] : NULL;
        $order->date_of_deposit = (isset($info_json['EGRN']['details']['Дата внесения стоимости'])) ? $info_json['EGRN']['details']['Дата внесения стоимости'] : NULL;
        $order->value_approval_date = (isset($info_json['EGRN']['details']['Дата утверждения стоимости'])) ? $info_json['EGRN']['details']['Дата утверждения стоимости'] : NULL;
        $order->date_of_information_update = (isset($info_json['EGRN']['details']['Дата обновления информации'])) ? $info_json['EGRN']['details']['Дата обновления информации'] : NULL;
        $order->type_of_ownership = (isset($info_json['EGRN']['details']['Форма собственности'])) ? $info_json['EGRN']['details']['Форма собственности'] : NULL;
        $order->number_of_rights_holders = (isset($info_json['EGRN']['details']['Количество правообладателей'])) ? $info_json['EGRN']['details']['Количество правообладателей'] : NULL;
        $order->cadastral_engineer = (isset($info_json['EGRN']['details']['Кадастровый инженер'])) ? $info_json['EGRN']['details']['Кадастровый инженер'] : NULL;
        $order->encoded_object = (isset($info_json['encoded_object'])) ? $info_json['encoded_object'] : NULL;        
        $order->status = 0;
        $order->XZP = ($xzp == 'true') ? 1 : 0;
        $order->SOPP = ($sopp == 'true') ? 1 : 0;
        $order->order_sum = 0;
        $order->order_sum  += ($order->XZP == 1) ? Yii::$app->params['price_egrp'] : 0;
        $order->order_sum  += ($order->SOPP == 1) ? Yii::$app->params['price_access'] : 0;
        $order->order_sum = (string)$order->order_sum;
        $order->zip_file = ($zip_file == 'true') ? 1 : 0;
        $order->human_file = ($human_file == 'true') ? 1 : 0;
        $order->xml_file = ($xml_file == 'true') ? 1 : 0;
        
        if($order->validate()) {
            if($order->save()) {
                $mrh_login = Yii::$app->params['sMerchantLogin'];
                $mrh_pass1 = Yii::$app->params['sMerchantPass1'];
                $isTest = Yii::$app->params['isTest'];
                $inv_id = $order->id;
                $out_summ = $order->order_sum;
                $in_curr = "";
                $culture = "ru";
                $encoding = "utf-8";
                // generate signature
                $crc = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass1");
                $url = "https://auth.robokassa.ru/Merchant/Index.aspx?MrchLogin=$mrh_login&" .
                    "OutSum=$out_summ&InvId=$inv_id&SignatureValue=$crc&IsTest=$isTest&Encoding=$encoding" .
                    "&Culture=$culture&IncCurrLabel=$in_curr";
                echo json_encode(['status' => true, 'url' => $url]);
            } else {
                echo json_encode(['status' => false, 'errors' => 'В процессе сохранения заказа произошла ошибка.']);
            }
        } else {
            echo json_encode(['status' => false, 'errors' => $order->errors]);
        }
        Yii::$app->end();
    }
    
    public function actions()
    {
        return [
            'result' => [
                'class' => '\robokassa\ResultAction',
                'callback' => [$this, 'resultCallback'],
            ],
            'success' => [
                'class' => '\robokassa\SuccessAction',
                'callback' => [$this, 'successCallback'],
            ],
            'fail' => [
                'class' => '\robokassa\FailAction',
                'callback' => [$this, 'failCallback'],
            ],
        ];
    }
    
    /**
     * Callback.
     * @param \robokassa\Merchant $merchant merchant.
     * @param integer $nInvId invoice ID.
     * @param float $nOutSum sum.
     * @param array $shp user attributes.
     */
    public function successCallback($merchant, $nInvId, $nOutSum, $shp)
    {
        $model = $this->loadModel($nInvId);
        $model->status = 2;
        if($model->save()) {
            $mail = Yii::$app->phpmailer;
            $mail->addAddress($model->email, '');
            $mail->Subject = Yii::$app->params['success_payment'];
            $mail->Body = $this->renderPartial('/../mail/templates/success_payment', [
                'cadnumber' => $model->CADNOMER,
                'objectaddress' => $model->ADDRESS,
                'order_id' => $model->id,
            ]);
            $mail->send();
        }
        return $this->redirect("/rosreestr?query={$model->CADNOMER}&payment=success");
    }
    public function resultCallback($merchant, $nInvId, $nOutSum, $shp)
    {
        $model = $this->loadModel($nInvId);
        $model->status = 1;
        $model->save();
        return 'OK' . $nInvId;
    }
    public function failCallback($merchant, $nInvId, $nOutSum, $shp)
    {
        $model = $this->loadModel($nInvId);
        $model->status = -1;
        if($model->save()) {
            $mail = Yii::$app->phpmailer;
            $mail->addAddress($model->email, '');
            $mail->Subject = Yii::$app->params['fail_payment'];
            $mail->Body = $this->renderPartial('/../mail/templates/fail_payment', [
                'cadnumber' => $model->CADNOMER,
                'objectaddress' => $model->ADDRESS,
                'order_id' => $model->id,
            ]);
            $mail->send();
        }
        return $this->redirect("/rosreestr?query={$model->CADNOMER}&payment=fail");
    }
    
    /**
     * @param integer $id
     * @return Order
     * @throws \yii\web\BadRequestHttpException
     */
    protected function loadModel($id) {
        $model = Order::find()->where(['id' => $id])->one();
        if ($model === null) {
            throw new BadRequestHttpException;
        }
        return $model;
    }
    
}