/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
        config.extraPlugins = 'lineheight';
        config.fillEmptyBlocks = false;
        config.enterMode = CKEDITOR.ENTER_BR;
        config.allowedContent = true;
        config.contentsCss = '/css/fonts.css';
        //the next line add the new font to the combobox in CKEditor
        config.font_names = 'OpenSans-Regular/OpenSans-Regular;OpenSans-Bold/OpenSans-Bold;OpenSans-ExtraBold/OpenSans-ExtraBold;' + config.font_names;
        config.line_height="1em;1.1em;1.2em;1.3em;1.4em;1.5em" ;
    }
