var info_json;

$(document).ready(function () {
    
    $(".example-link").bind('click', function () {
        var _content = $(this).text();
        $(".address-input").val(_content);
        return false;
    });

    $('input[type=checkbox]').iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
        increaseArea: '20%' // optional
    });
    
    $("#nextStep").bind('click', function(){
        if($('#doc_type_xzp').prop("checked") == false && $('#doc_type_sopp').prop("checked") == false) {
            show_info_modal('Ощибка', "Вы не выбрали ни одного документа");
            return false;
        }
        $("#beforePayForm").modal('show');
    });
    
    $("#payBtn").bind('click', function(){
        var xzp = $("#doc_type_xzp").prop("checked");
        var sopp = $("#doc_type_sopp").prop("checked");
        var email = $("#email_contact").val();
        var zip_file = $("#zip_file").prop("checked");
        var human_file = $("#human_file").prop("checked");
        var xml_file = $("#xml_file").prop("checked");
        if(email == '') {
            show_info_modal('Ошибка', 'Не заполнено поле EMAIL. Мы не знаем куда высылать готовые файлы.');
            return false;
        }

        if(zip_file == false && human_file == false && xml_file == false) {
            show_info_modal('Ошибка', 'Не выбран ни один формат. В каком формате вам подготовить файлы?');
            return false;
        }
        
        $.ajax({
            url: '/robokassa/register-payment',
            method: 'post',
            data: {
                    info_json: info_json, 
                    xzp: xzp, 
                    sopp: sopp, 
                    email: email, 
                    zip_file: zip_file, 
                    human_file: human_file, 
                    xml_file: xml_file
                },
            dataType: 'json',
            success: function(response) {
                console.log(response['status']);
                if(response['status'] == true) {
                	window.location.href=response.url;
                } else {
                	errors = '';
                	$.each(response['errors'], function(i, field){
                		$.each(field, function(j, error){
                			errors = errors+'<li>'+error+'</li>';
                		})
                	});
                	errors = '<ul class="text-danger">'+errors+'</ul>';
                	show_info_modal('Произошли ошибки', errors);
                } 
                	
            }
        });
        
    });
});

function show_info_modal(title, content) {
    $("#info_modal_title").empty().append(title);
    $("#info_modal_body").empty().append(content);
    $("#info_modal").modal('show');
}


