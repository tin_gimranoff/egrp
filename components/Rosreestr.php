<?php

namespace app\components;

use Yii;
use yii\base\Component;

class Rosreestr extends Component {

    public function api($class, $params = [], $token = 'RUME-INZ2-Q5HA-FPPF') {
        $class = strtolower($class);
        $ch = curl_init();
        curl_setopt_array($ch, [
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => ["Token: $token"],
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_FORBID_REUSE => 1,
            CURLOPT_VERBOSE => 1,
            CURLOPT_SSL_VERIFYPEER => 0,
        ]);

        curl_setopt($ch, CURLOPT_URL, "http://apirosreestr.ru/api/$class");
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));

        $exec = curl_exec($ch);
        $data = json_decode($exec, 1);
        if ($data && $data['error']) {
            //$data['error']['code'], $data['error']['mess']
            return $data;
        }
        curl_close($ch);
        return $data ? : $exec;
    }

}
