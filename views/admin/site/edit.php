<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Редактирование настроек сайта';
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>
    <? if(!empty($model->errors)):?>
    <div class="row">
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Внимание!</h4>
            <ul>
                <? foreach($model->errors as $key=>$field): ?>
                    <? foreach($field as $e):?>
                        <li><?= $e ?></li>
                    <? endforeach; ?>
                <? endforeach; ?>
            </ul>
        </div>
    </div>
    <? endif; ?>

    <?php
    $form = ActiveForm::begin([
                'options' => ['class' => 'form-horizontal'],
            ])
    ?>
    <div class="row">
        <?=
        $form->field($model, 'admin_password', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions' => [ 'class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control'],
        ])->passwordInput();
        ?>
        <?=
        $form->field($model, 'price_egrp', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions' => [ 'class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control'],
        ]);
        ?>
        <?=
        $form->field($model, 'price_access', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions' => [ 'class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control'],
        ]);
        ?>
        <?=
        $form->field($model, 'sMerchantLogin', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions' => [ 'class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control'],
        ]);
        ?>
        <?=
        $form->field($model, 'sMerchantPass1', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions' => [ 'class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control'],
        ]);
        ?>
        <?=
        $form->field($model, 'sMerchantPass2', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions' => [ 'class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control'],
        ]);
        ?>
        <?=
        $form->field($model, 'isTest', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions' => [ 'class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control'],
        ])->dropDownList([0 => 'Нет', 1 => 'Да']);
        ?>
        <?=
        $form->field($model, 'Host', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions' => [ 'class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control'],
        ]);
        ?>
        <?=
        $form->field($model, 'Port', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions' => [ 'class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control'],
        ]);
        ?>
        <?=
        $form->field($model, 'Username', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions' => [ 'class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control'],
        ]);
        ?>
        <?=
        $form->field($model, 'Password', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions' => [ 'class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control', 'id' => 'contacts_text'],
        ]);
        ?>
        <?=
        $form->field($model, 'FromEmail', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions' => [ 'class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control'],
        ]);
        ?>
        <?=
        $form->field($model, 'FromName', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions' => [ 'class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control'],
        ]);
        ?>
        <?=
        $form->field($model, 'files_topic_msg', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions' => [ 'class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control'],
        ]);
        ?>
        <?=
        $form->field($model, 'success_payment', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions' => [ 'class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control'],
        ]);
        ?>
        <?=
        $form->field($model, 'fail_payment', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions' => [ 'class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control'],
        ]);
        ?>
        <?=
        $form->field($model, 'save_order', [
            'template' => '{label}<div class="col-sm-10">{input}{error}{hint}</div>',
            'labelOptions' => [ 'class' => 'col-sm-2 control-label'],
            'inputOptions' => ['class' => 'form-control'],
        ]);
        ?>
        <div class="col-xs-12">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-primary btn-flat']) ?>
            <a href="/admin/site"><button type="button" class="btn btn-primary btn-flat">Отменить</button></a>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<script type="text/javascript">
    CKEDITOR.replace('header_text', {
    	 filebrowserBrowseUrl: '/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
         filebrowserUploadUrl: '/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
         filebrowserImageBrowseUrl: '/filemanager/dialog.php?type=1&editor=ckeditor&fldr=',
        });
    CKEDITOR.replace('contacts_text', {
    	 filebrowserBrowseUrl: '/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
         filebrowserUploadUrl: '/filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
         filebrowserImageBrowseUrl: '/filemanager/dialog.php?type=1&editor=ckeditor&fldr=',
        });
</script>
