<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Настройки сайта';
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

    <table class="table table-striped">
        <tr>
            <th>Параметр</th>
            <th>Значение</th>
        </tr>
        <tr>
            <td>Стоимость выписки из ЕГРП</td>
            <td><?= $model->price_egrp ?></td>
        </tr>
        <tr>
            <td>Стоимость выписки о переходе прав</td>
            <td><?= $model->price_access ?></td>
        </tr>
        <tr>
            <td>Идентификатор магазина в Робокассе</td>
            <td><?= $model->sMerchantLogin ?></td>
        </tr>
        <tr>
            <td>Пароль #1 в Робокассе</td>
            <td><?= $model->sMerchantPass1 ?></td>
        </tr>
        <tr>
            <td>Пароль #2 в Робокассе</td>
            <td><?= $model->sMerchantPass2 ?></td>
        </tr>
        <tr>
            <td>Режим тестирования</td>
            <td><?= ($model->isTest == 1) ? 'V' : 'X' ?></td>
        </tr>
        <tr>
            <td>Хост почтового сервера</td>
            <td><?= $model->Host ?></td>
        </tr>
        <tr>
            <td>Порт почтового сервера</td>
            <td><?= $model->Port ?></td>
        </tr>
        <tr>
            <td>Юзер почтового сервера</td>
            <td><?= $model->Username ?></td>
        </tr>
        <tr>
            <td>Пароль почтового сервера</td>
            <td><?= $model->Password ?></td>
        </tr>
        <tr>
            <td>Email от</td>
            <td><?= $model->FromEmail ?></td>
        </tr>
        <tr>
            <td>Имя от</td>
            <td><?= $model->FromName ?></td>
        </tr>
        <tr>
            <td>Топик сообщения при отсылки файлов</td>
            <td><?= $model->files_topic_msg ?></td>
        </tr>
        <tr>
            <td>Топик сообщения при успешной оплате</td>
            <td><?= $model->success_payment ?></td>
        </tr>
        <tr>
            <td>Топик сообщения при оказе от оплаты</td>
            <td><?= $model->fail_payment ?></td>
        </tr>
        <tr>
            <td>Топик сообщения при начале работы по оформлению файлов</td>
            <td><?= $model->save_order ?></td>
        </tr>
    </table>
    <a class="btn btn-primary" href="/admin/site/edit">Редактировать настройки</a>
</div>
