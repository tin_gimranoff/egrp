<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Заказы';
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>
<div class="table-responsive">
    <?= \yii\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-striped'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'email',
            'region',
            'CADNOMER',
            'ADDRESS',
            'AREA',
            'CATEGORY',
            'object_type',
            'object_status',
            'date_of_cadastral_registration',
            'land_category',
            'permitted_use',
            'area_num',
            'unit',
            'cadastral_sum',
            'value_date',
            'date_of_deposit',
            'value_approval_date',
            'date_of_information_update',
            'type_of_ownership',
            'number_of_rights_holders',
            'cadastral_engineer',
            'order_sum',
            [
                'label' => 'Статус заказа',
                'value' => function($data) {
                    if($data->status == -1) return 'Ошибка при оплате';
                    if($data->status == 0) return 'Черновой заказ';
                    if($data->status == 1) return 'Оплата поступила';
                    if($data->status == 2) return 'Успешно оплачен';
                    if($data->status == 3) return 'Отправлен в Росреестр';
                    if($data->status == 4) return 'Обработан в ЕГРП и можно скачивать файлы';
                    if($data->status == 5) return 'Можно отправлять клиенту';
                    if($data->status == 6) return 'Файлы отправлены клиенту';
                },
            ],
            [
                'label' => 'Выписка из ЕГРН об объекте недвижимости(XZP)',
                'value' => function($data) {
                    return ($data->XZP == 1) ? 'V' : '';
                },
            ],
            [
                'label' => 'Выписка о переходе прав на объект недвижимости(SOPP)',
                'value' => function($data) {
                    return ($data->SOPP == 1) ? 'V' : '';
                },
            ],
            [
                'label' => 'ZIP',
                'value' => function($data) {
                    return ($data->zip_file == 1) ? 'V' : '';
                },
            ],
            [
                'label' => 'PDF',
                'value' => function($data) {
                    return ($data->human_file == 1) ? 'V' : '';
                },
            ],
            [
                'label' => 'XML',
                'value' => function($data) {
                    return ($data->xml_file == 1) ? 'V' : '';
                },
            ],
            'transaction_id',
            'XZP_ID',
            'SOPP_ID',
            [
                'label' => 'Статус документа XZP',
                'value' => function($data) {
                    if($data->XZP_STATUS == 2) return 'Заявка сохранена';
                    if($data->XZP_STATUS == 3) return 'В работе';
                    if($data->XZP_STATUS == 4) return 'Выполнен';
                    if($data->XZP_STATUS == 5) return 'Ошибка при обработке';
                    if($data->XZP_STATUS == 6) return 'Отменен';
                    if($data->XZP_STATUS == 9) return 'Файл скачан';
                },
            ],
            [
                'label' => 'Статус документа XZP',
                'value' => function($data) {
                    if($data->SOPP_STATUS == 2) return 'Заявка сохранена';
                    if($data->SOPP_STATUS == 3) return 'В работе';
                    if($data->SOPP_STATUS == 4) return 'Выполнен';
                    if($data->SOPP_STATUS == 5) return 'Ошибка при обработке';
                    if($data->SOPP_STATUS == 6) return 'Отменен';
                    if($data->SOPP_STATUS == 9) return 'Файл скачан';
                },
            ],
 
            [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{update} {delete}',
            ],
        ],
    ]); ?>
</div>
</div>
