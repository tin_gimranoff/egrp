<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use app\assets\AppAssetAdmin;

AppAssetAdmin::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
        <script type="text/javascript" src="/ckeditor/ckeditor.js?v=0005"></script>
        <style>
            body { margin-bottom: 25px; margin-top: 55px; }
        </style>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <div class="wrap">
            <?php
            NavBar::begin([
                'brandLabel' => 'ЕГРП',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar-default navbar-fixed-top',
                ],
            ]);
            echo Nav::widget([
                'options' => ['class' => 'nav navbar-nav navbar-right'],
                'items' => [
                    [
                        'label' => 'Основные настройки',
                        'url' => '/admin/site',
                        'visible' => Yii::$app->user->isGuest === false,
                    ],
                    [
                        'label' => 'Заказы',
                        'url' => '/admin/orders',
                        'visible' => Yii::$app->user->isGuest === false,
                    ],
                    [
                        'label' => 'Настройки',
                        'url' => '/admin/site',
                        'visible' => Yii::$app->user->isGuest === false,
                        'items' => [
                            [
                                'label' => 'Настройка меню',
                                'url' => '/admin/menu',
                                'visible' => Yii::$app->user->isGuest === false,
                            ],
                        ],
                    ],
                    [
                        'label' => 'Текстовые страницы',
                        'visible' => Yii::$app->user->isGuest === false,
                        'items' => [
                            [
                                'label' => 'Страницы',
                                'url' => '/admin/articles',
                                'visible' => Yii::$app->user->isGuest === false,
                            ],
                        ],
                    ],
                    [
                        'label' => 'SEO',
                        'visible' => Yii::$app->user->isGuest === false,
                        'items' => [
                            [
                                'label' => 'Список',
                                'url' => '/admin/seo',
                                'visible' => Yii::$app->user->isGuest === false,
                            ],
                            [
                                'label' => 'Новый элемент',
                                'url' => '/admin/seo/update',
                                'visible' => Yii::$app->user->isGuest === false,
                            ],
                        ],
                    ],
                    [
                        'label' => 'Выход',
                        'url' => '/admin/site/logout',
                        'visible' => Yii::$app->user->isGuest === false,
                    ]
                ],
            ]);
            NavBar::end();
            ?>

            <div class="container">
                <?= $content ?>
            </div>
        </div>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
