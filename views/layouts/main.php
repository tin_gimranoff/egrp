<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
$main_menu = app\models\Menu::find()->orderBy('position ASC')->all();
$main_menu_arr = [];
foreach($main_menu as $m) {
    $main_menu_arr[] =
        [
                'label' => $m->name,
                'url' => $m->link,
        ];
}
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <? $descr = \app\models\Seo::find()->where(['url' => $_SERVER['REQUEST_URI']])->one();?>
    <?($descr && !empty($descr->meta_description)) ? $_d = $descr->meta_description : $_d = '' ?>
    <meta name="description" content="<?= $_d ?>">
    <? $keywords = \app\models\Seo::find()->where(['url' => $_SERVER['REQUEST_URI']])->one();?>
    <?($keywords && !empty($keywords->meta_keywords)) ? $_k = $keywords->meta_keywords : $_k = '' ?>
    <meta name="keywords" content="<?= $_k ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <? $title = \app\models\Seo::find()->where(['url' => $_SERVER['REQUEST_URI']])->one();?>
    <?($title && !empty($title->title)) ? $_t = $title->title . ' &mdash; Выписки из ЕГРН' : $_t = Html::encode($this->title) . ' &mdash; Выписки из ЕГРН' ?>
    <title><?= $_t ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => $main_menu_arr,
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; ЕГРП <?= date('Y') ?></p>

        <p class="pull-right"></p>
    </div>
</footer>

<div class="modal fade" tabindex="-1" role="dialog" id="info_modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="info_modal_title">Modal title</h4>
      </div>
      <div class="modal-body">
        <p id="info_modal_body">One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
