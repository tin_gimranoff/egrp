<?php
/* @var $this yii\web\View */
?>
<div class="site-index">
    <div class="hero-unit hero-unit-inner">
        <form action="/rosreestr">
            <input type="text" class="form-control address-input" placeholder="Для заказа выписки введите адрес или кадастровый номер" name="query" value="<?= $query ?>"/>
            <input type="submit" class="btn btn-success address-btn" value="Найти">
        </form>
        <p>Например, <a href="#" class="example-link">77:08:0009005:1596</a> или <a href="#" class="example-link">Москва Левобережная дом 11 стр.2</a></p>
    </div>

    <div class="body-content">
        <div class="container">
            <h2>
                <small style="line-height: 120%; display: block; margin: 0; padding: 0;">Кадастровый номер: <?= $info['EGRN']['object']['CADNOMER'] ?></small>
                <?= $info['EGRN']['details']['Адрес (местоположение)'] ?>
            </h2>
            <div class="object-info">
                <span>Кадастровый номер — <?= $info['EGRN']['object']['CADNOMER'] ?></span><br />
                <?php if (isset($info['EGRN']['details']['(ОКС) Тип'])): ?>
                    <span>Тип — <?= $info['EGRN']['details']['(ОКС) Тип'] ?></span><br />
                <?php endif; ?>
                <?php if (isset($info['EGRN']['object']['AREA'])): ?>
                    <span>Площадь — <?= $info['EGRN']['object']['AREA'] ?></span><br />
                <?php endif; ?>
                <?php if (isset($info['EGRN']['details']['Этаж'])): ?>
                    <span>Этаж — <?= $info['EGRN']['details']['Этаж'] ?></span><br /><br />
                <?php endif ?>
                <?php if (isset($info['EGRN']['details']['Адрес (местоположение)'])): ?>
                    <span><strong>Адрес:</strong><br /><?= $info['EGRN']['details']['Адрес (местоположение)'] ?></span>
                <?php endif ?>
                <br /><br />
                <div class="table-responsive col-lg-10">
                    <table class="table table-sm doc-order-table">
                        <tr>
                            <th></th>
                            <th>Документ</th>
                            <th>Цена</th>
                            <th>Срок</th>
                            <th>Образец</th>
                        </tr>
                        <tr>
                            <td><input type="checkbox" name="doc_type[XZP]" id="doc_type_xzp"></td>
                            <td><strong>Выписка из ЕГРН</strong><br />ФИО собственника квартиры, кадастровая стоимость, наличие/отсутствие обременений/аренды/ипотеки.</td>
                            <td><?php echo Yii::$app->params['price_egrp'] ?> руб.</td>
                            <td>за 5-30 минут</td>
                            <td><a data-fancybox="gallery" href="/img/egrn_kvartira.png">Образец</a></td>
                        </tr>
                        <tr>
                            <td><input type="checkbox" name="doc_type[SOPP]" id="doc_type_sopp"></td>
                            <td><strong>Выписка из ЕГРН о переходе прав</strong><br />История собственников (по сделкам, совершенным с 1998 года).</td>
                            <td><?php echo Yii::$app->params['price_access'] ?> руб.</td>
                            <td>за 5-30 минут</td>
                            <td><a data-fancybox="gallery" href="/img/egrn_perehod.png">Образец</a></td>
                        </tr>
                    </table>
                </div>
                <div class="clearfix"></div>
                <div class="text-center">
                    <input type="submit" class="btn btn-success btn-lg" value="Заказать документы" style="text-align: center" id="nextStep">
                </div>
                <div class="pay_icons">
                    <p style="text-align:center;margin-top:1em;">
                        <img src="/img/pay_icons1.png" alt="Способы оплаты Visa, Mastercard, МИР, Киви">
                        <img src="/img/pay_icons2.png" alt="Способы оплаты Webmoney, Яндекс.Деньги, Мегафон, МТС, Билайн">
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="beforePayForm">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Заполните форму для завершения заказа</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="">Куда отправить документы и оповещать о статусе?</label>
                        <input type="email" class="form-control" id="email_contact" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="">Форматы файлов, которые хотите получить</label>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="zip_file"> Оригинальный файл с ЭЦП Росреестра<br />
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="human_file"> Человеко-читаемый формат <br />
                            </label>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" id="xml_file"> XML — из распакованного ZIP файла<br />
                            </label>
                        </div>
                    </div>            
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Отменить</button>
                <button type="button" class="btn btn-primary" id="payBtn">Оплатить</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php if (isset($error) && !empty($error)): ?>
    <script type="text/javascript">
        window.onload = function () {
            show_info_modal('Ошибка', 'В процессе запроса возникла системная ошибка: <?php echo $error ?>');
        }
    </script>
<?php endif; ?>




