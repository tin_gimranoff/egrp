<?php
/* @var $this yii\web\View */
?>
<div class="site-index">

    <div class="hero-unit hero-unit-inner">
        <form action="/rosreestr">
            <input type="text" class="form-control address-input" placeholder="Для заказа выписки введите адрес или кадастровый номер" name="query" value="<?= $query ?>"/>
            <input type="submit" class="btn btn-success address-btn" value="Найти">
        </form>
        <p>Например, <a href="#" class="example-link">77:08:0009005:1596</a> или <a href="#" class="example-link">Москва Левобережная дом 11 стр.2</a></p>
    </div>

    <div class="body-content">
        <div class="container">
            <h2>
                <small style="line-height: 120%; display: block; margin: 0; padding: 0;">Объекты найденные по запросу</small>
                <?= $query ?>
            </h2>
            <div class="object-info">
                <div class="table-responsive col-lg-10">
                    <table class="table table-striped table-sm doc-order-table">
                        <tr>
                            <th>Кадастровый номер</th>
                            <th>Адрес объекта</th>
                            <th></th>
                            <th></th>
                        </tr>
                        <?php if(isset($objects)): foreach($objects['objects'] as $o): ?>
                            <tr>
                                <td><?= $o['CADNOMER'] ?></td>
                                <td>
                                    <?= $o['ADDRESS'] ?><br />
                                    <span class="small"><?= $o['TYPE']?></span>
                                </td>
                                <td><?= $o['AREA'] ?></td>
                                <td>
                                    <a href="/rosreestr?query=<?= $o['CADNOMER']  ?>">
                                        <input type="button" class="btn btn-success" value="Выбрать">
                                    </a>
                                </td>
                            </tr>
                        <?php endforeach; endif; ?>
                    </table>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>


<?php if (isset($error) && !empty($error)): ?>
    <script type="text/javascript">
        window.onload = function () {
            show_info_modal('Ошибка', 'В процессе запроса возникла системная ошибка: <?php echo $error ?>');
        }
    </script>
<?php endif; ?>

