<?php

/* @var $this yii\web\View */

$this->title = 'ЕГРП';
?>
<div class="site-index">

    <div class="hero-unit">
        <h1>Срочные выписки из ЕГРН</h1>
        <p class="lead">Выписки из ЕГРН on-line</p>
        <form action="/rosreestr">
            <input type="text" class="form-control address-input" placeholder="Для заказа выписки введите адрес или кадастровый номер" name="query"/>
            <input type="submit" class="btn btn-success address-btn" value="Найти">
        </form>
        <p>Например, <a href="#" class="example-link">77:08:0009005:1596</a> или <a href="#" class="example-link">Москва Левобережная дом 11 стр.2</a></p>
    </div>

    <div class="body-content">
        <div class="row">
            <?php foreach ($articles as $a): ?>
                <div class="col-lg-4">
                    <h2><?php echo $a->name; ?></h2>

                    <p><?php echo mb_substr(strip_tags(trim(html_entity_decode($a->content, ENT_QUOTES, 'UTF-8'), "\xc2\xa0")), 0, 300, 'UTF-8') ?></p>

                    <p><a class="btn btn-default" href="<?php echo $a->url ?>">Подробнее</a></p>
                </div>
            <?php endforeach;?>
        </div>

    </div>
</div>
