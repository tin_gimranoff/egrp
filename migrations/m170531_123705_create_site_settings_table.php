<?php

use yii\db\Migration;

/**
 * Handles the creation of table `site_settings`.
 */
class m170531_123705_create_site_settings_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('site_settings', [
            'id' => $this->primaryKey(),
            'price_egrp' => $this->decimal(15, 2)->defaultValue(1)->comment('Стоимость выписки из ЕГРП'),
            'price_access' => $this->decimal(15, 2)->defaultValue(1)->comment('Стоимость выписки о переходе прав'),
            'sMerchantLogin' => $this->string()->notNull()->comment('Идентификатор магазина в Робокассе'),
            'sMerchantPass1' => $this->string()->notNull()->comment('Пароль #1 в Робокассе'),
            'sMerchantPass2' => $this->string()->notNull()->comment('Пароль #2 в Робокассе'),
            'isTest' => $this->boolean()->defaultValue(true)->comment('Режим тестирования'),
            'Host' => $this->string()->notNull()->comment('Хост почтового сервера'),
            'Port' => $this->integer()->notNull()->comment('Хост почтового сервера'),
            'Username' => $this->string()->notNull()->comment('Юзер почтового сервера'),
            'Password' => $this->string()->notNull()->comment('Пароль почтового сервера'),
            'FromEmail' => $this->string()->notNull()->comment('Email от'),
            'FromName' => $this->string()->notNull()->comment('Имя от'),
            'files_topic_msg' => $this->string()->notNull()->comment('Топик сообщения при отсылки файлов'),
            'success_payment' => $this->string()->notNull()->comment('Топик сообщения при успешной оплате'),
            'fail_payment' => $this->string()->notNull()->comment('Топик сообщения при оказе от оплаты'),
            'save_order' => $this->string()->notNull()->comment('Топик сообщения при начале работы по оформлению файлов'),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('site_settings');
    }
}
