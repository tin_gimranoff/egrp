<?php

use yii\db\Migration;

/**
 * Handles the creation of table `seo`.
 */
class m170623_103458_create_seo_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('seo', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->defaultValue(NULL),
            'meta_keywords' => $this->string()->defaultValue(NULL),
            'meta_description' => $this->text()->defaultValue(NULL),
            'url' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('seo');
    }
}
