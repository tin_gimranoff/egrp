<?php

use yii\db\Migration;

/**
 * Class m180723_144117_alter_number_of_rihgts
 */
class m180723_144117_alter_number_of_rihgts extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('orders', 'number_of_rights_holders', $this->string(255)->defaultValue(NULL)->comment('Количество правообладателей'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('orders', 'number_of_rights_holders', $this->integer()->defaultValue(NULL)->comment('Количество правообладателей'));
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180723_144117_alter_number_of_rihgts cannot be reverted.\n";

        return false;
    }
    */
}
