<?php

use yii\db\Migration;

/**
 * Class m180705_201048_add_columns_to_orders
 */
class m180705_201048_add_columns_to_orders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders', 'transaction_id', $this->integer()->defaultValue(NULL)->comment('ID транзакции'));
        $this->addColumn('orders', 'XZP_ID', $this->integer()->defaultValue(NULL)->comment('ID документа XZP'));
        $this->addColumn('orders', 'SOPP_ID', $this->integer()->defaultValue(NULL)->comment('ID документа SOPP'));
        $this->addColumn('orders', 'XZP_STATUS', $this->integer()->defaultValue(NULL)->comment('Статус документа XZP'));
        $this->addColumn('orders', 'SOPP_STATUS', $this->integer()->defaultValue(NULL)->comment('Статус документа SOPP'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180705_201048_add_columns_to_orders cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180705_201048_add_columns_to_orders cannot be reverted.\n";

        return false;
    }
    */
}
