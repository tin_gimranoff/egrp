<?php

use yii\db\Migration;

/**
 * Class m180702_184700_orders
 */
class m180702_184700_orders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('orders', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->defaultValue(NULL)->comment('Email для связи'),
            'region' => $this->string()->notNull()->comment('Регион'),            
            'CADNOMER' => $this->string()->defaultValue(NULL)->comment('Кадастровый номер'),
            'ADDRESS' => $this->string()->defaultValue(NULL)->comment('Адрес'),
            'AREA' => $this->string()->defaultValue(NULL)->comment('Площадь'),
            'CATEGORY' => $this->string()->defaultValue(NULL)->comment('Категория'),
            'object_type' => $this->string()->defaultValue(NULL)->comment('Тип объекта'),
            'object_status' => $this->string()->defaultValue(NULL)->comment('Статус объекта'),
            'date_of_cadastral_registration' => $this->date()->defaultValue(NULL)->comment('Дата постановки на кадастровый учет'),
            'land_category' => $this->string()->defaultValue(NULL)->comment('Категория земель'),
            'permitted_use' => $this->string()->defaultValue(NULL)->comment('Разрешенное использование'),
            'area_num' => $this->string()->defaultValue(NULL)->comment('Площадь цифрой'),
            'unit' => $this->string()->defaultValue(NULL)->comment('Единица измерения (Код)'),
            'cadastral_sum' => $this->string()->defaultValue(NULL)->comment('Кадастровая стоимость'),
            'value_date' => $this->date()->defaultValue(NULL)->comment('Дата определения стоимости'),
            'date_of_deposit' => $this->date()->defaultValue(NULL)->comment('Дата внесения стоимости'),
            'value_approval_date' => $this->date()->defaultValue(NULL)->comment('Дата утверждения стоимости'),
            'date_of_information_update' => $this->date()->defaultValue(NULL)->comment('Дата обновления информации'),
            'type_of_ownership' => $this->string()->defaultValue(NULL)->comment('Форма собственности'),
            'number_of_rights_holders' => $this->integer()->defaultValue(0)->comment('Количество правообладателей'),
            'cadastral_engineer' => $this->string()->defaultValue(NULL)->comment('Кадастровый инженер'),
            'encoded_object' => $this->text()->notNull(),
            'order_sum' => $this->string()->notNull()->comment('Сумма заказа'),
            'status' => $this->integer(1)->defaultValue(0)->comment('Статус заказа'),
            'XZP' => $this->boolean()->defaultValue(FALSE)->comment('Выписка из ЕГРН об объекте недвижимости'),
            'SOPP' => $this->boolean()->defaultValue(FALSE)->comment('Выписка о переходе прав на объект недвижимости'),
            'zip_file' => $this->boolean()->defaultValue(FALSE)->comment('ZIP'),
            'human_file' => $this->boolean()->defaultValue(FALSE)->comment('Человекопочитаемый файл'),
            'xml_file' => $this->boolean()->defaultValue(FALSE)->comment('XML файл'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('orders');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180702_184700_orders cannot be reverted.\n";

        return false;
    }
    */
}
