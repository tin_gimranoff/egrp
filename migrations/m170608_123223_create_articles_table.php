<?php

use yii\db\Migration;

/**
 * Handles the creation of table `articles`.
 */
class m170608_123223_create_articles_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('articles', [
            'id' => $this->primaryKey()->comment('ID'),
            'name' => $this->string()->notNull()->comment('Название'),
            'content' => $this->text()->notNull()->comment('Содержимое'),
            'url' => $this->string()->unique()->notNull()->comment('URL страницы'),
            'on_main' => $this->boolean()->defaultValue(false)->comment('Показывать на главной')
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('articles');
    }
}
