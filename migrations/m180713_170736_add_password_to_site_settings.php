<?php

use yii\db\Migration;

/**
 * Class m180713_170736_add_password_to_site_settings
 */
class m180713_170736_add_password_to_site_settings extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('site_settings', 'admin_password', $this->string()->notNull()->comment('Админский пароль'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('site_settings', 'admin_password');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180713_170736_add_password_to_site_settings cannot be reverted.\n";

        return false;
    }
    */
}
