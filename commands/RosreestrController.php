<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\components\MyMailer;
use Yii;
use app\models\Order;
use yii\console\Controller;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class RosreestrController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionSaveOrder()
    {
        $orders = Order::find()->where(['status' => 1])->orWhere(['status' => 2])->orderBy('id ASC')->limit(100)->all();
        $api = Yii::$app->rosreestr;
        foreach($orders as $order) {
            $documents = [];
            if($order->XZP == 1)
                $documents[] = 'XZP';
            if($order->SOPP == 1)
                $documents[] = 'SOPP';
            $status = $api->api('Cadaster/Save_order', [
                'encoded_object' => $order->encoded_object,
                'documents' => $documents,
            ]);
            if(count($status['error']) == 0) {
                $order->transaction_id = $status['transaction_id'];
                if(isset($status['documents_id']['XZP'])) {
                    $order->XZP_ID = $status['documents_id']['XZP'];
                }
                if(isset($status['documents_id']['SOPP'])) {
                    $order->SOPP_ID = $status['documents_id']['SOPP'];
                }
                $order->status = 3;
                if($order->save()) {
                    $mail = Yii::$app->phpmailer;
                    $mail->addAddress($order->email, '');
                    $mail->Subject = Yii::$app->params['save_order'];
                    $mail->Body = $this->renderPartial('/../mail/templates/save_order', [
                        'cadnumber' => $order->CADNOMER,
                        'objectaddress' => $order->ADDRESS,
                        'order_id' => $order->id,
                    ]);
                    $mail->send();
                }
            }
            sleep(10);
        }
    }

    public function actionGetOrder()
    {
        $orders = Order::find()->where(['status' => 3])->orderBy('id ASC')->limit(100)->all();
        $api = Yii::$app->rosreestr;
        foreach($orders as $order) {
            $status = $api->api('Cadaster/Orders', [
                'id' => $order->transaction_id,
            ]);
            if(count($status['error']) == 0) {
                foreach ($status['documents'] as $document) {
                    if($document['type'] == 'XZP')
                        $order->XZP_STATUS = $document['status'];
                    if($document['type'] == 'SOPP')
                        $order->SOPP_STATUS = $document['status'];
                }
                $order->save();
                $completed = true;
                if($order->XZP == 1 && $order->XZP_STATUS != 4)
                    $completed = false;
                if($order->SOPP == 1 && $order->SOPP_STATUS != 4)
                    $completed = false;
                if($completed == true) {
                    $order->status = 4;
                    $order->save();
                }
            }
        }
        sleep(10);
    }

    public function actionDownloadFiles() {
        $orders = Order::find()->where(['status' => 4])->orderBy('id ASC')->limit(10)->all();
        $api = Yii::$app->rosreestr;
        foreach ($orders as $order) {
            $dir = Yii::$app->basePath . '/web/orders/' . $order->id . '_' . $order->transaction_id;
            if(!is_dir($dir))
                mkdir($dir);
            if($order->XZP == 1 && $order->XZP_STATUS == 4) {
                if($order->zip_file == 1) {
                    $this->saveFile($api, $dir, $order->XZP_ID, 'ZIP');
                }
                if($order->human_file == 1) {
                    $this->saveFile($api, $dir, $order->XZP_ID, 'PDF');
                }
                if($order->xml_file == 1) {
                    $this->saveFile($api, $dir, $order->XZP_ID, 'XML');
                }
                $order->XZP_STATUS = 9;
                $order->save();
            }
            if($order->SOPP == 1 && $order->SOPP_STATUS == 4) {
                if($order->zip_file == 1) {
                    $this->saveFile($api, $dir, $order->SOPP_ID, 'ZIP');
                }
                if($order->human_file == 1) {
                    $this->saveFile($api, $dir, $order->SOPP_ID, 'PDF');
                }
                if($order->xml_file == 1) {
                    $this->saveFile($api, $dir, $order->SOPP_ID, 'XML');
                }
                $order->SOPP_STATUS = 9;
                $order->save();
            }
            $completed = true;
            if($order->XZP == 1 && $order->XZP_STATUS != 9)
                $completed = false;
            if($order->SOPP == 1 && $order->SOPP_STATUS != 9)
                $completed = false;
            if($completed == true) {
                $order->status = 5;
                $order->save();
            }
            sleep(10);
        }
    }

    public function actionSendFiles() {

        $orders = Order::find()->where(['status' => 5])->limit(10)->all();

        foreach($orders as $order) {
            $mail = Yii::$app->phpmailer;
            $mail->addAddress($order->email, '');
            $mail->Subject = Yii::$app->params['files_topic_msg'];
            $mail->Body = $this->renderPartial('/../mail/templates/sendfiles', [
                'cadnumber' => $order->CADNOMER,
                'objectaddress' => $order->ADDRESS,
                'order_id' => $order->id,
            ]);
            $dir = Yii::$app->basePath . '/web/orders/' . $order->id . '_' . $order->transaction_id;
            if($order->XZP_STATUS == 9) {
                if($order->zip_file == 1)
                    $mail->addAttachment($dir . '/' . $order->XZP_ID . '.zip');
                if($order->human_file == 1)
                    $mail->addAttachment($dir . '/' . $order->XZP_ID . '.pdf');
                if($order->xml_file == 1)
                    $mail->addAttachment($dir . '/' . $order->XZP_ID . '.xml');
            }
            if($order->SOPP_STATUS == 9) {
                if($order->zip_file == 1)
                    $mail->addAttachment($dir . '/' . $order->SOPP_ID . '.zip');
                if($order->human_file == 1)
                    $mail->addAttachment($dir . '/' . $order->SOPP_ID . '.pdf');
                if($order->xml_file == 1)
                    $mail->addAttachment($dir . '/' . $order->SOPP_ID . '.xml');
            }
            if($mail->send()) {
                $order->status = 6;
                $order->save();
            }
            $mail->ClearAddresses();
            $mail->ClearAttachments();
            sleep(10);
        }

    }

    public function actionCheckOrder($transaction_id = NULL) {
        $api = Yii::$app->rosreestr;
        $status = $api->api('Cadaster/Orders', [
            'id' => $transaction_id,
        ]);
        var_dump($status);
    }

    private function saveFile($api, $dir, $doc_id, $format) {
        $file = $api->api('Cadaster/Download', [
            'document_id' => $doc_id,
            'format' => $format,
        ]);
        $handle = fopen($dir . '/' . $doc_id . '.' . strtolower($format), 'wb');
        fwrite($handle, $file);
        fclose($handle);
    }
}
