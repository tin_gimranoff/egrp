<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "site_settings".
 *
 * @property int $id
 * @property string $price_egrp Стоимость выписки из ЕГРП
 * @property string $price_access Стоимость выписки о переходе прав
 * @property string $sMerchantLogin Идентификатор магазина в Робокассе
 * @property string $sMerchantPass1 Пароль #1 в Робокассе
 * @property string $sMerchantPass2 Пароль #2 в Робокассе
 * @property int $isTest Режим тестирования
 * @property string $Host Хост почтового сервера
 * @property int $Port Хост почтового сервера
 * @property string $Username Юзер почтового сервера
 * @property string $Password Пароль почтового сервера
 * @property string $FromEmail Email от
 * @property string $FromName Имя от
 * @property string $files_topic_msg Топик сообщения при отсылки файлов
 * @property string $success_payment Топик сообщения при успешной оплате
 * @property string $fail_payment Топик сообщения при оказе от оплаты
 * @property string $save_order Топик сообщения при начале работы по оформлению файлов
 * @property string $admin_password Админский пароль
 */
class SiteSettings extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'site_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price_egrp', 'price_access'], 'number'],
            [['sMerchantLogin', 'sMerchantPass1', 'sMerchantPass2', 'Host', 'Port', 'Username', 'Password', 'FromEmail', 'FromName', 'files_topic_msg', 'success_payment', 'fail_payment', 'save_order', 'admin_password'], 'required'],
            [['isTest', 'Port'], 'integer'],
            [['sMerchantLogin', 'sMerchantPass1', 'sMerchantPass2', 'Host', 'Username', 'Password', 'FromEmail', 'FromName', 'files_topic_msg', 'success_payment', 'fail_payment', 'save_order', 'admin_password'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price_egrp' => 'Стоимость выписки из ЕГРП',
            'price_access' => 'Стоимость выписки о переходе прав',
            'sMerchantLogin' => 'Идентификатор магазина в Робокассе',
            'sMerchantPass1' => 'Пароль #1 в Робокассе',
            'sMerchantPass2' => 'Пароль #2 в Робокассе',
            'isTest' => 'Режим тестирования',
            'Host' => 'Хост почтового сервера',
            'Port' => 'Хост почтового сервера',
            'Username' => 'Юзер почтового сервера',
            'Password' => 'Пароль почтового сервера',
            'FromEmail' => 'Email от',
            'FromName' => 'Имя от',
            'files_topic_msg' => 'Топик сообщения при отсылки файлов',
            'success_payment' => 'Топик сообщения при успешной оплате',
            'fail_payment' => 'Топик сообщения при оказе от оплаты',
            'save_order' => 'Топик сообщения при начале работы по оформлению файлов',
            'admin_password' => 'Админский пароль',
        ];
    }
}
