<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property string $email Email для связи
 * @property string $region Регион
 * @property string $CADNOMER Кадастровый номер
 * @property string $ADDRESS Адрес
 * @property string $AREA Площадь
 * @property string $CATEGORY Категория
 * @property string $object_type Тип объекта
 * @property string $object_status Статус объекта
 * @property string $date_of_cadastral_registration Дата постановки на кадастровый учет
 * @property string $land_category Категория земель
 * @property string $permitted_use Разрешенное использование
 * @property string $area_num Площадь цифрой
 * @property string $unit Единица измерения (Код)
 * @property string $cadastral_sum Кадастровая стоимость
 * @property string $value_date Дата определения стоимости
 * @property string $date_of_deposit Дата внесения стоимости
 * @property string $value_approval_date Дата утверждения стоимости
 * @property string $date_of_information_update Дата обновления информации
 * @property string $type_of_ownership Форма собственности
 * @property string $number_of_rights_holders Количество правообладателей
 * @property string $cadastral_engineer Кадастровый инженер
 * @property text $encoded_object
 * @property string $order_sum Сумма заказа
 * @property int $status Статус заказа
 * @property int $XZP Выписка из ЕГРН об объекте недвижимости
 * @property int $SOPP Выписка о переходе прав на объект недвижимости
 * @property int $zip_file ZIP
 * @property int $human_file Человекопочитаемый файл
 * @property int $xml_file XML файл
 * @property int $transaction_id ID транзакции
 * @property int $XZP_ID ID документа XZP
 * @property int $SOPP_ID ID документа SOPP
 * @property int $XZP_STATUS Статус документа XZP
 * @property int $SOPP_STATUS Статус документа SOPP
 */

//Статусы заказа
//-1 - Ошибка при оплате
//0 - Черновой заказ
//1 - Оплата поступила
//2 - Оплачен
//3 - Отправлен в ЕГРП
//4 - Обработан в ЕГРП и можно скачивать файлы
//5 - Можно отправлять клиенту
//6 - Файлы отправлены клиенту

//Статусы документов
//Статус документа
//2 — Заявка сохранена
//3 — В работе
//4 — Выполнен
//5 — Ошибка при обработке
//6 — Отменен
//9 - Файл скачан
class Order extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['region', 'encoded_object', 'order_sum'], 'required'],
            [['date_of_cadastral_registration', 'value_date', 'date_of_deposit', 'value_approval_date', 'date_of_information_update', 'encoded_object'], 'safe'],
            [['status', 'XZP', 'SOPP', 'zip_file', 'human_file', 'xml_file', 'transaction_id', 'XZP_ID', 'SOPP_ID', 'XZP_STATUS', 'SOPP_STATUS'], 'integer'],
            [['number_of_rights_holders', 'cadastral_sum', 'order_sum', 'area_num', 'email', 'region', 'CADNOMER', 'ADDRESS', 'AREA', 'CATEGORY', 'object_type', 'object_status', 'land_category', 'permitted_use', 'unit', 'type_of_ownership', 'cadastral_engineer'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email для связи',
            'region' => 'Регион',
            'CADNOMER' => 'Кадастровый номер',
            'ADDRESS' => 'Адрес',
            'AREA' => 'Площадь',
            'CATEGORY' => 'Категория',
            'object_type' => 'Тип объекта',
            'object_status' => 'Статус объекта',
            'date_of_cadastral_registration' => 'Дата постановки на кадастровый учет',
            'land_category' => 'Категория земель',
            'permitted_use' => 'Разрешенное использование',
            'area_num' => 'Площадь цифрой',
            'unit' => 'Единица измерения (Код)',
            'cadastral_sum' => 'Кадастровая стоимость',
            'value_date' => 'Дата определения стоимости',
            'date_of_deposit' => 'Дата внесения стоимости',
            'value_approval_date' => 'Дата утверждения стоимости',
            'date_of_information_update' => 'Дата обновления информации',
            'type_of_ownership' => 'Форма собственности',
            'number_of_rights_holders' => 'Количество правообладателей',
            'cadastral_engineer' => 'Кадастровый инженер',
            'encoded_object' => 'Encoded Object',
            'order_sum' => 'Сумма заказа',
            'status' => 'Статус заказа',
            'XZP' => 'Выписка из ЕГРН об объекте недвижимости',
            'SOPP' => 'Выписка о переходе прав на объект недвижимости',
            'zip_file' => 'ZIP',
            'human_file' => 'Человекопочитаемый файл',
            'xml_file' => 'XML файл',
            'transaction_id' => 'ID транзакции',
            'XZP_ID' => 'ID документа XZP',
            'SOPP_ID' => 'ID документа SOPP',
            'XZP_STATUS' => 'Статус документа XZP',
            'SOPP_STATUS' => 'Статус документа SOPP',
        ];
    }
}
